const express = require('express');
const morgan = require('morgan');

const cache = require('./service/cache');

const app = express();
const port = 3000;

// HTTP Logger
app.use(morgan('tiny'));

// Static files
app.use(express.static('public'));

// Access request body
app.use(express.json());

// Routes
app.use('/import', require('./routes/import'));
app.use('/item', require('./routes/item'));
app.use('*', (req, res) => {
	res.sendFile(__dirname + '/public/index.html');
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
