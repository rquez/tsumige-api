const {body} = require("express-validator");
const ISO6391 = require('iso-639-1');
const cache = require('../service/cache');

const dateRegex = /\d{4}-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])*/;
const yearValidator = (y) => y > 1950 && y < 3000;

const importRelease = [
	body('*.title.*', 'Language code must be in ISO6391')
		.exists()
		.custom(l => ISO6391.validate(l)),
	body('*.title', 'Title must contain text values')
		.not()
		.isEmpty(),
	body('*.title.*.text', 'Title text must not be empty')
		.not()
		.isEmpty(),
	body(['item.year', 'release.year'], 'Invalid item year')
		.exists()
		.custom(yearValidator),
	body('item.type', 'Invalid item type')
		.exists()
		.custom(t => !!cache.get('TYPES')[t])
		.customSanitizer(p => cache.get('TYPES')[p].id),
	body('release.platform', 'Invalid release platform')
		.exists()
		.custom(p => !!cache.get('PLATFORMS')[p])
		.customSanitizer(p => cache.get('PLATFORMS')[p].id),
	body('release.players', 'Invalid format')
		.optional()
		.matches(/^[1-9][PA]$/),
	body(['release.completed', 'release.physical'], 'Boolean value required')
		.exists()
		.isBoolean(),
	body(['release.dateCompleted', 'release.dateUpdated'], 'Date must be in yyyy-mm-dd format')
		.optional()
		.matches(dateRegex),
	body('release.dateUpdated', 'Date must be in yyyy-mm-dd format')
		.optional()
		.matches(dateRegex),
	body('release.imageLink', 'Image link must be a URL')
		.optional()
		.isURL(),
	body('release.link.link', 'Link must be a URL')
		.optional()
		.isURL(),
	body('release.link.source', 'Link must have a source name')
		.if(body('release.link.link').exists())
		.not()
		.isEmpty()
];

module.exports = {
	importRelease
};
