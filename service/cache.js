const NodeCache = require('node-cache');
const myCache = new NodeCache();

const get = (key) => {
	return myCache.get(key);
};

module.exports = {
	get
};
