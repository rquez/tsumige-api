const Router = require('express-promise-router');

const itemDAO = require('../db/item');

const router = new Router();

router.get('/', async (req, res) => {

	// Select all items
	const items = await itemDAO.getItems();

	await res.json(items);
});

module.exports = router;
