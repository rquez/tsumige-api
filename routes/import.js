const Franchise = require("../models/franchise");
const Item = require("../models/item");
const Release = require("../models/release");

const {validationResult} = require("express-validator");
const Router = require('express-promise-router');
const validator = require('../service/validator');

const franchiseDAO = require('../db/franchise');
const itemDAO = require('../db/item');
const releaseDAO = require('../db/release');

const router = new Router();

router.post('/', [], async (req, res) => {

	// Validate body
	/*
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({ errors: errors.array() });
	}
	 */

	// Add franchise
	const franchise = Franchise.from(req.body.franchise);
	franchise.id = await franchiseDAO.addFranchise(franchise);

	//  Add item, add it to the release
	const item = Item.from(req.body.item);
	item.franchise = franchise;
	console.log('PASSING THIS ITEM', item);
	item.id = await itemDAO.addItem(item);

	// Add release
	const release = Release.from(req.body.release);
	release.id = await releaseDAO.addRelease(release, item.id);

	// Mark as successful import
	req.body.status = 'SUCCESS';

	await res.json({
		franchise,
		item,
		release,
		status: 'SUCCESS',
		position: req.body.position,
	});
});

module.exports = router;
