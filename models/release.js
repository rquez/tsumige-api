module.exports = class Release {

	constructor(
		id,
		title,
		platform,
		progress,
		players,
		stock,
		physical,
		completed,
		dateCompleted,
		dateUpdated,
		imageLink,
		link,
		year
	) {
		this._id = id;
		this._title = title;
		this._platform = platform;
		this._progress = progress;
		this._players = players;
		this._stock = stock;
		this._physical = physical;
		this._completed = completed;
		this._dateCompleted = dateCompleted;
		this._dateUpdated = dateUpdated;
		this._imageLink = imageLink;
		this._link = link;
		this._year = year;
	}

	get id() {
		return this._id;
	}

	set id(value) {
		this._id = value;
	}

	get title() {
		return this._title;
	}

	set title(value) {
		this._title = value;
	}

	get platform() {
		return this._platform;
	}

	set platform(value) {
		this._platform = value;
	}

	get progress() {
		return this._progress;
	}

	set progress(value) {
		this._progress = value;
	}

	get players() {
		return this._players;
	}

	set players(value) {
		this._players = value;
	}

	get stock() {
		return this._stock;
	}

	set stock(value) {
		this._stock = value;
	}

	get physical() {
		return this._physical;
	}

	set physical(value) {
		this._physical = value;
	}

	get completed() {
		return this._completed;
	}

	set completed(value) {
		this._completed = value;
	}

	get dateCompleted() {
		return this._dateCompleted;
	}

	set dateCompleted(value) {
		this._dateCompleted = value;
	}

	get dateUpdated() {
		return this._dateUpdated;
	}

	set dateUpdated(value) {
		this._dateUpdated = value;
	}

	get imageLink() {
		return this._imageLink;
	}

	set imageLink(value) {
		this._imageLink = value;
	}

	get link() {
		return this._link;
	}

	set link(value) {
		this._link = value;
	}

	get year() {
		return this._year;
	}

	set year(value) {
		this._year = value;
	}

	static from(json){
		return Object.assign(new Release(), json);
	}
}
