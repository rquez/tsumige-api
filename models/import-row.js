module.exports = class ImportRow {

	constructor(franchise, item, release, position, status) {
		this._franchise = franchise;
		this._item = item;
		this._release = release;
		this._position = position;
		this._status = status;
	}

	get franchise() {
		return this._franchise;
	}

	set franchise(value) {
		this._franchise = value;
	}

	get item() {
		return this._item;
	}

	set item(value) {
		this._item = value;
	}

	get release() {
		return this._release;
	}

	set release(value) {
		this._release = value;
	}

	get position() {
		return this._position;
	}

	set position(value) {
		this._position = value;
	}

	get status() {
		return this._status;
	}

	set status(value) {
		this._status = value;
	}

	static from(json){
		return Object.assign(new ImportRow(), json);
	}
}
