module.exports = class Item {

	constructor(id, title, year, type, franchise, releases) {
		this._id = id;
		this._title = title;
		this._year = year;
		this._type = type;
		this._franchise = franchise;
		this._releases = releases;
	}

	get id() {
		return this._id;
	}

	set id(value) {
		this._id = value;
	}

	get title() {
		return this._title;
	}

	set title(value) {
		this._title = value;
	}

	get year() {
		return this._year;
	}

	set year(value) {
		this._year = value;
	}

	get type() {
		return this._type;
	}

	set type(value) {
		this._type = value;
	}

	get franchise() {
		return this._franchise;
	}

	set franchise(value) {
		this._franchise = value;
	}

	get releases() {
		return this._releases;
	}

	set releases(value) {
		this._releases = value;
	}

	static from(json){
		return Object.assign(new Item(), json);
	}
}
