const pool = require('../db');
const titleDAO = require('./title');

const GET_SERIES_ID_BY_TITLE = `
SELECT S.ID
FROM TITLE TI
INNER JOIN SERIES S ON S.TITLE_ID = TI.ID
INNER JOIN TITLE_TEXT TT ON TT.TITLE_ID = TI.ID
INNER JOIN TEXT T ON T.ID = TT.TEXT_ID
WHERE 1=1
AND S.FRANCHISE_ID = $1
AND TEXT = $2
AND RESOURCE = 'SERIES'
`;
const getSeriesIdByTitle = async (franchiseId, title) => {
	return await pool.queryReturnFirstColumn(GET_SERIES_ID_BY_TITLE, [franchiseId, title], 'id');
};

const INSERT_SERIES = 'INSERT INTO SERIES(TITLE_ID, FRANCHISE_ID) VALUES($1, $2) RETURNING ID';
const addSeries = async (series) => {

	// Check if series exists
	series.id = await getSeriesIdByTitle(series.franchise.id, series.title[0].text);

	// Create new series
	if (!series.id) {
		const sTitleId = await titleDAO.addTitleText(series.title, 'SERIES');
		series.id = await pool.queryReturnFirstColumn(INSERT_SERIES, [sTitleId, series.franchise.id], 'id');
	}

	return series;
};

const EXISTS_ITEM_ITEM_SERIES_ENTRY = `
SELECT EXISTS
(SELECT 1
FROM ITEM_ITEM_SERIES
WHERE SERIES_ID = $1
AND ITEM_ID = $2
AND REPLAY = false)
`;
const INSERT_ITEM_ITEM_SERIES = `
INSERT INTO ITEM_ITEM_SERIES(SERIES_ID, ITEM_ID, PARENT_ID, RELEASE_ID, REPLAY)
VALUES($1, $2, $3, $4, $5)
`;
const addItemSeries = async(seriesId, itemId, parentId, replay) => {

	// Check if this entry already exists
	const exists = await pool.queryExists(EXISTS_ITEM_ITEM_SERIES_ENTRY, [seriesId, itemId]);

	// Add this entry if it doesn't exist or this is a replay
	if (!exists || replay) {
		await  pool.query(INSERT_ITEM_ITEM_SERIES, [
			seriesId,
			itemId,
			parentId,
			replay
		]);
	}
};

module.exports = {
	getSeriesIdByTitle,
	addItemSeries,
	addSeries
};
