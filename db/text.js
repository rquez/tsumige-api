const pool = require('../db');

const INSERT_TEXT = 'INSERT INTO TEXT(LANG, TEXT) VALUES($1, $2) RETURNING ID';
const addText = async (lang, text) => {
	return await pool.queryReturnFirstColumn(INSERT_TEXT, [lang, text], 'id');
};

module.exports = {
	addText
};
