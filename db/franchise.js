const pool = require('../db');
const titleDAO = require('./title');

const GET_FRANCHISE_ID_BY_TITLE_ID = `
SELECT F.ID
FROM FRANCHISE F
WHERE F.TITLE_ID = $1
`;
const getFranchiseIdByTitleId = async (titleId) => {
	return await pool.queryReturnFirstColumn(GET_FRANCHISE_ID_BY_TITLE_ID, [titleId], 'id');
};

const INSERT_FRANCHISE = 'INSERT INTO FRANCHISE(TITLE_ID) VALUES($1) RETURNING ID';
const addFranchise = async (franchise) => {

	// Check if franchise title exists
	const franchiseTitleId = await titleDAO.getTitleIdsByLangText(franchise.title, 'FRANCHISE');

	// There was multiple matches for this or it was a null result, so make a new franchise
	if (franchiseTitleId.length > 1 || !franchiseTitleId[0]) {
		console.log('NEW FRANCHISE: ' + JSON.stringify(franchise.title));
		const titleId = await titleDAO.addTitleText(franchise.title, 'FRANCHISE');
		franchise.id = await pool.queryReturnFirstColumn(INSERT_FRANCHISE, [titleId], 'id');
		console.log('NEW FRANCHISE CREATED: ' + franchise.id);
	} else {
		franchise.id = await getFranchiseIdByTitleId(franchiseTitleId[0]);
		console.log('FOUND FRANCHISE: ' + franchise.id);
	}

	return franchise.id;
};

module.exports = {
	getFranchiseIdByTitleId,
	addFranchise
};
