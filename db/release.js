const pool = require('../db');
const titleDAO = require('./title');
const linkDAO = require('./link');

const INSERT_RELEASE = `
INSERT INTO RELEASE(
TITLE_ID,
YEAR,
PLATFORM,
STOCK,
PROGRESS,
PLAYERS,
PHYSICAL,
COMPLETED,
DATE_COMPLETED,
DATE_UPDATED,
IMAGE_LINK,
LINK_ID
)
VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
RETURNING ID
`;
const INSERT_ITEM_RELEASE = 'INSERT INTO ITEM_RELEASE(ITEM_ID, RELEASE_ID) VALUES($1, $2) RETURNING ID';
const addRelease = async (release, itemId) => {

	release.titleId = await titleDAO.addTitleText(release.title, 'RELEASE');
	console.log('NEW RELEASE TITLE: ' + JSON.stringify(release.title));

	// Check if release has a link
	if (release.link.link && release.link.source) {
		// Create new link for this release
		release.linkId = await linkDAO.addLink(release.link.link, release.link.source);
		console.log('NEW RELEASE LINK: ' + JSON.stringify(release.link));
	}

	release.id = await pool.queryReturnFirstColumn(
		INSERT_RELEASE,
		[
			release.titleId,
			release.year,
			release.platform,
			release.stock,
			release.progress,
			release.players,
			release.physical,
			release.completed,
			release.dateCompleted,
			release.dateUpdated,
			release.imageLink,
			release.linkId
		],
		'id'
	);

	// Link item to release
	await pool.query(INSERT_ITEM_RELEASE, [itemId, release.id]);
	console.log('NEW RELEASE: ' + JSON.stringify(release.id));

	return release.id;
};

module.exports = {
	addRelease
};
