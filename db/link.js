const pool = require('../db');

const INSERT_LINK = 'INSERT INTO LINK(LINK, SOURCE) VALUES($1, $2) RETURNING ID';
const addLink = async (link, source) => {
	return await pool.queryReturnFirstColumn(INSERT_LINK, [link, source], 'id');
};

module.exports = {
	addLink
};
