const { Pool } = require('pg');
const ramda = require('ramda');

const pool = new Pool({
	user: 'postgres',
	database: 'postgres',
	password: 'Password1234!',
	port: 5432,
});

module.exports = {
	query: async (text, params) => {
		try {
			await pool.query(text, params)
		} catch (error) {
			console.error(`Error in query '${text}' with params '${params}'`);
			throw error;
		}
	},
	queryReturnFirst: async (text, params) => {
		try {
			const { rows } = await pool.query(text, params);
			return ramda.head(rows);
		} catch (error) {
			console.error(`Error in query '${text}' with params '${params}'`);
			throw error;
		}
	},
	queryReturnFirstColumn: async (text, params, column) => {
		try {
			const { rows } = await pool.query(text, params);
			const row = ramda.head(rows);
			if (row) {
				return row[column];
			} else {
				return null;
			}
		} catch (error) {
			console.error(`Error in query '${text}' with params '${params}'`);
			throw error;
		}
	},
	queryReturn: async (text, params) => {
		try {
			const { rows } = await pool.query(text, params);
			return rows;
		} catch (error) {
			console.error(`Error in query '${text}' with params '${params}'`);
			throw error;
		}
	},
	queryExists: async (text, params) => {
		try {
			const { rows } = await pool.query(text, params);
			return rows[0][0] === 'true';
		} catch (error) {
			console.error(`Error in query '${text}' with params '${params}'`);
			throw error;
		}
	},
	queryReturnMap: async (text, params, keyColumn) => {
		try {
			const { rows } = await pool.query(text, params);

			const map = {};
			// Format rows [{column}] to { keyColumn: [{column}] }
			rows.forEach(r => {
				const key = r[keyColumn];

				// Add to key's array if it already exists
				if (map[key]) {
					map[key].push(r);
				} else {
					map[key] = [r];
				}
			});

			return map;
		} catch (error) {
			console.error(`Error in query '${text}' with params '${params}'`);
			throw error;
		}
	},
	exists: async (text, params) => {
		const { rows } = await pool.query(text, params);
		return (ramda.head(rows)).exists === 'true';
	},
};
