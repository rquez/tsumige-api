const pool = require('../db');
const textDAO = require('./text');

const GET_TITLE_ID_BY_LANG_TEXT = `
SELECT TITLE_ID FROM TEXT
INNER JOIN TITLE_TEXT ON TEXT.ID = TITLE_TEXT.TEXT_ID
INNER JOIN TITLE ON TITLE.ID = TITLE_TEXT.TITLE_ID
WHERE LANG = $1 AND TEXT = $2 AND RESOURCE = $3`;
const getTitleIdsByLangText = async (title, resource) => {
	// Get all the title ids for this text by language
	const ids = await Promise.all(
		Object.keys(title).map(lang => {
			return pool.queryReturnFirstColumn(GET_TITLE_ID_BY_LANG_TEXT, [lang, title[lang], resource], 'title_id');
		})
	);

	// Return a unique array of title ids, there should only be 1 element, if not then this title is new
	return ids.filter((id, i, ids) => ids.indexOf(id) === i);
};

const INSERT_TITLE = 'INSERT INTO TITLE(RESOURCE) VALUES($1) RETURNING ID';
const addTitle = async (resource) => {
	return await pool.queryReturnFirstColumn(INSERT_TITLE, [resource], 'id');
};

const INSERT_TITLE_TEXT = 'INSERT INTO TITLE_TEXT(TITLE_ID, TEXT_ID) VALUES($1, $2)';
const addTitleText = async (titles, resource) => {
	const titleId = await addTitle(resource);
	for await (const tl of Object.keys(titles)) {
		const textId = await textDAO.addText(tl, titles[tl]);
		await pool.query(INSERT_TITLE_TEXT, [titleId, textId]);
	}
	return titleId;
};

module.exports = {
	addTitle,
	addTitleText,
	getTitleIdsByLangText
};
