const pool = require('../db');
const titleDAO = require('./title');

const GET_ITEM_ID_BY_METADATA = `
SELECT ID
FROM ITEM I
WHERE 1=1
AND FRANCHISE_ID = $1
AND YEAR = $2
AND TYPE = $3
`;
const getItemIdByMetadata = async (item) => {
	return await pool.queryReturnFirstColumn(GET_ITEM_ID_BY_METADATA, [item.franchise.id, item.year, item.type], 'id');
};

const INSERT_ITEM = `INSERT INTO ITEM(TITLE_ID, FRANCHISE_ID, YEAR, TYPE) VALUES($1, $2, $3, $4) RETURNING ID`;
const addItem = async (item) => {

	// Check if item title exists
	const itemTitleId = await titleDAO.getTitleIdsByLangText(item.title, 'ITEM');

	// Check if item metadata exists
	const itemId = await getItemIdByMetadata(item);

	// Create new item, if it doesn't exist
	if (itemTitleId.length > 1 || !itemTitleId[0] || !itemId) {
		console.log('NEW ITEM: ' + JSON.stringify(item.title));
		const itemTitleId = await titleDAO.addTitleText(item.title, 'ITEM');
		item.id = await pool.queryReturnFirstColumn(
			INSERT_ITEM,
			[itemTitleId, item.franchise.id, item.year, item.type],
			'id'
		);
		console.log('NEW ITEM CREATED: ' + item.id);
	} else {
		item.id = itemId;
		console.log('FOUND ITEM: ' + item.id);
	}

	return item.id;
};

const GET_RELEASES = `
SELECT
	R.ID,
	IR.ITEM_ID AS ITEM_ID,
	R.PLATFORM,
	R.PROGRESS,
	R.PLAYERS,
	R.STOCK,
	R.PHYSICAL,
	R.COMPLETED,
	R.DATE_COMPLETED,
	R.DATE_UPDATED,
	R.YEAR,
	R.IMAGE_LINK,
	L.LINK,
	L.SOURCE,
	XR.TEXT,
	XR.LANG,
	R.PLATFORM
FROM ITEM_RELEASE IR
INNER JOIN RELEASE R ON
	R.ID = IR.RELEASE_ID
INNER JOIN TITLE_TEXT TTR ON
	TTR.TITLE_ID = R.TITLE_ID
INNER JOIN TEXT XR ON
	XR.ID = TTR.TEXT_ID
INNER JOIN LINK L ON
	L.ID = R.LINK_ID
`;

const GET_ITEMS = `
SELECT
	I.ID,
	XI.TEXT,
	XI.LANG,
	I.YEAR,
	I.FRANCHISE_ID,
	I.TYPE
FROM ITEM I
INNER JOIN TITLE_TEXT TTI ON
	TTI.TITLE_ID = I.TITLE_ID
INNER JOIN TEXT XI ON
	XI.ID = TTI.TEXT_ID
`;

const GET_FRANCHISES = `
SELECT
	F.ID,
	XF.TEXT,
	XF.LANG
FROM FRANCHISE F
INNER JOIN TITLE_TEXT TTF ON
	TTF.TITLE_ID = F.TITLE_ID
INNER JOIN TEXT XF ON
	XF.ID = TTF.TEXT_ID
`;

const getItems = async () => {

	const franchises = (await pool.queryReturn(GET_FRANCHISES, []))
		.reduce((accum, row) => {
			if (accum[row.id] && !accum[row.id].title[row.lang]) {
				accum[row.id].title[row.lang] = row.text;
			} else {
				accum[row.id] = {
					title: {
						[row.lang]: row.text
					},
					id: row.id
				};
			}
			return accum;
		}, {});

	const releases = (await pool.queryReturn(GET_RELEASES, []))
		.reduce((accum, row) => {
			const r = accum.find(ar => ar.id === row.id);
			if (r && !r.title[row.lang]) {
				r.title[row.lang] = row.text;
			} else {
				accum.push({
					id: row.id,
					itemId: row.item_id,
					platform: row.platform,
					progress: row.progress,
					players: row.players,
					stock: row.stock,
					physical: row.physical,
					completed: row.completed,
					dateCompleted: row.date_completed,
					dateUpdated: row.date_updated,
					imageLink: row.image_link,
					year: +row.year,
					link: {
						link: row.link,
						source: row.source
					},
					title: {
						[row.lang]: row.text
					}
				})
			}
			return accum;
		}, []);

	return (await pool.queryReturn(GET_ITEMS, []))
		.reduce((accum, row) => {
			const i = accum.find(ai => ai.id === row.id);
			if (i && !i.title[row.lang]) {
				i.title[row.lang] = row.text;
			} else {
				accum.push({
					title: {
						[row.lang]: row.text
					},
					year: +row.year,
					type: row.type,
					id: row.id,
					franchise: franchises[row.franchise_id],
					releases: releases.filter(r => r.itemId === row.id)
				});
			}
			return accum;
		}, []);
};


module.exports = {
	getItemIdByMetadata,
	addItem,
	getItems
};
